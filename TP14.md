# TP 14 - LimitRange, ResourceQuota

```bash
mkdir ../TP14 && cd ../TP14
kubectl get nodes
kubectl describe node minikube | less
kubectl run my-pod --image=samiche92/my-node-server:1.0.0 --port=8080 -o yaml --dry-run=client > my-pod.yaml
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    ports:
    - containerPort: 8080
    resources:
      requests:
        memory: "500Mi"
        cpu: "1"
```

```bash
kubectl delete all --all
kubectl apply -f my-pod.yaml
kubectl describe node minikube | less
cp my-pod.yaml my-pod-2.yaml
vim my-pod-2.yaml
# name: my-pod => my-pod-2
kubectl apply -f my-pod-2.yaml
kubectl get pods
kubectl describe pod my-pod-2

vim my-lr.yaml
```

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: my-lr
spec:
  limits:
    - defaultRequest: # Default requests
        cpu: "100m"
        memory: "100Mi"
      default: # Default limits
        cpu: "1"
        memory: "500Mi"
      max:
        cpu: "2"
        memory: "1Gi"
      min:
        cpu: "100m"
        memory: "50Mi"
      type: Container
```

```bash
kubectl apply -f my-lr.yaml
kubectl get limitranges
vim my-pod-2.yaml 
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod-2
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    ports:
    - containerPort: 8080
```

```bash
kubectl delete -f my-pod-2.yaml ; kubectl apply -f my-pod-2.yaml 
kubectl get pods
kubectl describe node minikube | less

vim my-rq.yaml
```

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: my-rq
spec:
  hard:
    cpu: 1
    memory: 2Gi
    pods: 3
```

```bash
kubectl apply -f my-rq.yaml 
kubectl get resourcequotas
kubectl delete -f my-pod.yaml
kubectl get resourcequotas
kubectl apply -f my-pod.yaml
kubectl run my-pod-3 --image=samiche92/my-node-server:1.0.0
kubectl get resourcequotas
kubectl run my-pod-4 --image=samiche92/my-node-server:1.0.0
kubectl get resourcequotas
kubectl run my-pod-5 --image=samiche92/my-node-server:1.0.0
```
